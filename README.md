# Gobsmack aws London policy  

this specifies a policy that allows full access, but only to a specific region  
- in this case london - intention is to separate from Dublin which is production.  

going forward look at organisations but this is a bigger project.  

## Authors

* **Stuart Abrams-Humphries ** - *Initial work* - [gobsmack](https://bitbucket.com/)

## License

gobsmack
